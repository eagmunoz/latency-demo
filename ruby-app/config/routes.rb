Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resource :network_client
      resource :flow
    end
  end
end
