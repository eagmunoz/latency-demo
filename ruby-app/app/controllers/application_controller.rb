class ApplicationController < ActionController::Base
  after_action :set_origin

  protected

  def set_origin
    response.headers['Access-Control-Allow-Origin'] = '*'
  end
end
