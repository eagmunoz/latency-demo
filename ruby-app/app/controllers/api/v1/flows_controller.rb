class Api::V1::FlowsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    params['flows'].each do |flow|
      Flow.create({
        client_id: flow['client_addr'],
        dst_addr: flow['dst_addr'],
        syn_sec: flow['syn_sec'],
        syn_usec: flow['syn_usec'],
        ack_sec: flow['ack_sec'],
        ack_usec: flow['ack_usec'],
        syn_ack_sec: flow['syn_ack_sec'],
        syn_ack_usec: flow['syn_ack_usec']
      })
    end

    render :ok, json: @controller.to_json

  end


  def show
    client_id = params['client_id'].gsub('_', '.')
    time_now = Time.now.to_i - 10

    start_sequence = time_now - 120

    flows = Flow.where(client_id: client_id).where("syn_sec >= #{start_sequence}").where("syn_sec <= #{time_now}").order('syn_sec')

    timesteps = start_sequence.upto(time_now).to_a

    lan_delays = timesteps.map do |ts|
      ts_flows = flows.select do |f| f.flow_start == ts end
      ts_flows.size > 0 ? ts_flows.map do |ts_f| ts_f.lan_delay end.inject(0, :+) / ts_flows.size : 0
    end

    wan_delays = timesteps.map do |ts|
      ts_flows = flows.select do |f| f.flow_start == ts end
      ts_flows.size > 0 ? ts_flows.map do |ts_f| ts_f.wan_delay end.inject(0, :+) / ts_flows.size : 0
    end

    # render :ok, json: { flows: flows }.to_json
    r_json = timesteps.zip(lan_delays, wan_delays).map do |ts, lan_delay, wan_delay|
      {
        id: ts,
        lan_delay: lan_delay,
        wan_delay: wan_delay
      }
    end
    render :ok, json: { flows: r_json }.to_json
  end


end
