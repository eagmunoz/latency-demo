class Api::V1::NetworkClientsController < ApplicationController
  def show
    clients = Flow.group(:client_id).pluck(:client_id).compact

    client_delays = clients.map do |client|
      {
        lan_delay: Flow.where(client_id: client).last(50).map do |f| f.lan_delay end.compact,
        wan_delay: Flow.where(client_id: client).last(50).map do |f| f.wan_delay end.compact
      }
    end.map do |delays|
        {
          lan_delay: delays[:lan_delay].size > 0 ?  delays[:lan_delay].inject(0, :+) / delays[:lan_delay].size : 0,
          wan_delay: delays[:wan_delay].size > 0 ?  delays[:wan_delay].inject(0, :+) / delays[:wan_delay].size : 0
        }
    end
    json_r = clients.each_with_index.map do |client, i|
      { id: client.gsub('.', '_'), client_id: client, lan_delay: client_delays[i][:lan_delay], wan_delay: client_delays[i][:wan_delay] }
    end

    render :ok, json: { 'network-clients': json_r }
  end
end
