class Flow < ApplicationRecord
  def total_time(sec, usec)
    (usec + (sec * 1000000000.0)) / 1000000
  end

  def lan_delay
    total_time(self.ack_sec, self.ack_usec) - total_time(self.syn_ack_sec, self.syn_ack_usec)
  end

  def wan_delay
    total_time(self.syn_ack_sec, self.syn_ack_usec) - total_time(self.syn_sec, self.syn_usec)
  end

  def flow_start
    self.syn_sec
  end

  def flow_end
    self.ack_sec
  end
end
