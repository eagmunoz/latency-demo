require 'mqtt'
require 'json'
require 'httparty'
require 'pp'
require 'resolv'

topic = "netfilter-flow"
client = MQTT::Client.connect('127.0.0.1')
client.subscribe(topic)

# def total_time(sec, usec)
#   (usec + 0.0 + (sec * 1000000000.0)) / 1000000.0
# end

client.get do |_, message|
  msg = JSON.parse(message.gsub("'", '"'))
  flows_data = msg['flows'].select do |flow|
    flow['protocol'] == 6
  end.select do |flow|
    flow['syn_ack_sec'] != 0 && flow['ack_sec'] != 0
  end.map do |flow|
    dst_addr = flow['dst']

    begin
      dst_addr = Resolv.getname(dst_addr)
    rescue

    end

    # puts "#{flow['src']} #{flow['dst']} #{flow['syn_sec']} #{flow['syn_usec']} #{flow['ack_sec']} #{flow['ack_usec']} #{flow['syn_ack_sec']} #{flow['syn_ack_usec']}"
    {
      client_addr: flow['src'],
      dst_addr: dst_addr,
      syn_sec: flow['syn_sec'],
      syn_usec: flow['syn_usec'],
      ack_sec: flow['ack_sec'],
      ack_usec: flow['ack_usec'],
      syn_ack_sec: flow['syn_ack_sec'],
      syn_ack_usec: flow['syn_ack_usec']
    }
  end

  post_data = { flows: flows_data }

  if flows_data.size > 0
    HTTParty.post('http://127.0.0.1:3000/api/v1/flow', body: post_data)
  end
end
