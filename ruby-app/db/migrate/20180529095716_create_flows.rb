class CreateFlows < ActiveRecord::Migration[5.2]
  def change
    create_table :flows do |t|
      t.string :client_id
      t.integer :syn_sec
      t.integer :syn_usec
      t.integer :syn_ack_sec
      t.integer :syn_ack_usec
      t.integer :ack_sec
      t.integer :ack_usec
      t.timestamps
    end
  end
end
