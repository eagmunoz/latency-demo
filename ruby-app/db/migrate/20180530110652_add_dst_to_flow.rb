class AddDstToFlow < ActiveRecord::Migration[5.2]
  def change
    add_column :flows, :dst_addr, :string
  end
end
