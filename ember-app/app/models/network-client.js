import DS from 'ember-data';

export default DS.Model.extend({
  lan_delay: DS.attr('number'),
  wan_delay: DS.attr('number')
});
