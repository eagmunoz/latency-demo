import Controller from '@ember/controller';
import { computed } from '@ember/object';
import EmberObject from '@ember/object';

export default Controller.extend({
  init() {
    this._super(...arguments);
    this.set('lanSeries', []);
    this.set('wanSeries', []);
  },

  lanData: computed('lanSeries.[]', 'lanSeries', 'lanSeries.@each.delay', function() {
    return this.get('lanSeries').map( (dp) => {
      return { x: parseInt(dp['ts']) * 1000, y: dp['delay'], dst: undefined }
    });
  }),

  wanData: computed('wanSeries.[]', 'wanSeries', 'wanSeries.@each.delay', function() {
    return this.get('wanSeries').map( dp => {
      return {x: parseInt(dp['ts']) * 1000, y: -1 * dp['delay'], dst: undefined}
    });
  }),

  lanChartData: computed('lanData', function() {
    return [{ name: 'lan_delay', data: this.get('lanData') }]
  }),

  wanChartData: computed('wanData', function() {
    return [{ name: 'wan_delay', data: this.get('wanData') }]
  }),

  lanWanChartData: computed('wanChartData', 'lanChartData', function() {
    return [].concat(this.get('lanChartData'), this.get('wanChartData'));
  }),

  showChart: computed( 'lanData', 'wanData', function() {
    return this.get('lanData').length > 0 && this.get('wanData').length > 0;
  }),

  addNewResults(flows) {
    let lastLanValue = this.get('lanSeries.lastObject.delay');
    let lastWanValue = this.get('wanSeries.lastObject.delay');
    flows.forEach( (f) => {
      let lanObject = this.get('lanSeries').findBy('ts', f.id);
      let wanObject = this.get('wanSeries').findBy('ts', f.id);
      if (lanObject) {
        if (f.get('lan_delay') != 0) {
          lanObject.set('delay', f.get('lan_delay'));
        }
      } else {
        lastLanValue = f.get('lan_delay') || lastLanValue
        this.get('lanSeries').removeAt(0);
        this.get('lanSeries').addObject( new EmberObject({ts: f.id, delay: lastLanValue }));
      }

      if (wanObject) {
        if (f.get('wan_delay') != 0) {
          wanObject.set('delay', f.get('wan_delay'));
        }
      } else {
        lastWanValue = f.get('wan_delay') || lastWanValue
        this.get('wanSeries').removeAt(0);
        this.get('wanSeries').addObject( new EmberObject({ts: f.id, delay: lastWanValue }));
      }
    });
  },

  lanChartOptions: computed('model', function() {
    let ctrl = this;
    return {
      chart: {
        type: 'area',
        animation: Highcharts.svg,
        events: {
          load: () => {
            // set up the updating of the chart each second
            setInterval(() => {
              ctrl.get('model').store.query('flow', { client_id: ctrl.get('model.query.client_id') }).then( (r) => {
                this.addNewResults(r);
              });
            }, 1000);
          }
        }
      },

      title: {
        text: "Delays"
      },

      xAxis: { type: 'datetime' },

      tooltip: {
        formatter: function() {
          return `y: ${this.point.y}<br>dst: ${this.point.dst}`
        }
      }
    }
  }),

  wanChartOptions: computed('model', function() {
    return {
      title: {
        text: 'WAN Delay',
      },

      tooltip: {
        formatter: function() {
          return `y: ${this.point.y}<br>dst: ${this.point.dst}`
        }
      }
    }
  })
});
