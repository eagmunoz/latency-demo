import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({

  showPerformance: true,
  showBar: false,

  barData: computed('model', function() {
      return this.get('model').map( (client) => {
          return {
            name: client.id,
            data: [ client.get('lan_delay'), client.get('wan_delay') ]
          }
      })
  }),

  barOptions: computed('model', function() {
    return {
        chart: {
          type: 'bar',
        },
        title: {
          text: 'LAN/WAN Avg Delays'
        },
        xAxis: {
          categories: ['LAN', 'WAN']
        },
        yAxis: {
          title: {
            text: 'ms'
          }
        }
      };
  }),

  actions: {
    selectClient(client) {
      this.set('selectedClient', client)
      this.transitionToRoute('latencies.client', client.id );
    },

    showPerformance() {
      this.set('showPerformance', !this.get('showPerformance'));
      this.set('showBar', !this.get('showBar'));
    },
  }
});
