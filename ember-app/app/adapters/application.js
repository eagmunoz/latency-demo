import DS from 'ember-data';
import { underscore } from '@ember/string';
import { singularize } from 'ember-inflector';


export default DS.RESTAdapter.extend({
  namespace: 'api/v1',
  host: 'http://127.0.0.1:3000',

  pathForType(modelName) {
    return underscore(singularize(modelName))
  }
});
