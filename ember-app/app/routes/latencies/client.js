import Route from '@ember/routing/route';
import EmberObject from '@ember/object';

export default Route.extend({
  model(params) {
    return this.store.query('flow', { client_id: params.network_client_id });
  },

  setupController(controller, model) {
    controller.set('client_id', model.get('query.client_id'));
    controller.set('model', model);
    // controller.addNewResults(model);
    let lastLanValue = 0;
    let lastWanValue = 0;
    model.forEach( (f) => {
      lastLanValue = f.get('lan_delay') || lastLanValue;
      lastWanValue = f.get('wan_delay') || lastWanValue;
      controller.get('lanSeries').addObject( new EmberObject({ts: f.id, delay: lastLanValue }));
      controller.get('wanSeries').addObject( new EmberObject({ts: f.id, delay: lastWanValue }));
    });
  }
});
