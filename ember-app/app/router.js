import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('latencies', { path: '/latencies' }, function() {
    this.route('client', { path: '/:network_client_id' });
  });
});

export default Router;
